<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pithecantropus');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/xP{?KZ12$y(HOG*y]M_)(um0~`uoY$vQ/8#X 3D^=Uay{kMLRi!4,6)V{!.1Je&');
define('SECURE_AUTH_KEY',  '2byk-`Z^kw]Bw(+?#0-/[sLu8Ic<uaX3,Q<No#UB,,[&jE41&nuxpJ=`d^O/D6ek');
define('LOGGED_IN_KEY',    'i9O?.:FfKUK{1} -)) ],8?H*zCm5i=XniwSzYjPAxDgA`kPQ2-=[(ohHz/jcvAO');
define('NONCE_KEY',        'nQ+]><cn6q2UtbD+$Jub>T~omb|QH>4+x-Jf+K=zN!gK,lD&lCWFbAfJ[Hd+?3N_');
define('AUTH_SALT',        'p3$cI`P96v|*tg19Y+C!R]&2Tgs%YjhSqHR^6(bWpJkQmrG<=vt+^<tmK}KPMqGM');
define('SECURE_AUTH_SALT', '/p;T8y`Q4aANEs@]5ZYbNUc,=rN-W-@2;Eo{?}ajDz`?bubIDRd({I0xjTa,>Ywr');
define('LOGGED_IN_SALT',   'x!NtoN*FfC/[R8Q)Zm2Z#|jx(t~2<[#mlY kiAR%s3iTb`AzeO+wxo+^4 -p9<}c');
define('NONCE_SALT',       'lG_fIVM&BnrfQ+H+NL/NzEwjNgtr>HmnC.~stMNO?xROU.7R40+c2(KY]t}$qk/9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
